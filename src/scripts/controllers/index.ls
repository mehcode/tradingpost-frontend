'use strict'

require! chaplin
require! cookie

require! Controller: 'lib/controllers/header'

require! ListingView: 'views/listings'
require! OffersView: 'views/offers'
require! DiscussionView: 'views/discussion'

require! Listing: 'resources/product-offering'
require! Ticket: 'resources/ticket'

parse-query-string = (uri) ->
  query = {};
  uri.replace do
    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
    ($0, $1, $2, $3) -> query[$1] = $3
  query

module.exports = class IndexController extends Controller

  initialize: ->
    @listen-to chaplin.mediator.socket, 'create:catalog', (message) ->
      console.log "Got notified about a new product"
      chaplin.mediator.growl.log ["A new product has been posted:", message.data.name]
    @listen-to chaplin.mediator.socket, 'create:ticket', (message) ->
      console.log "Got notified about a new offer"

      person = undefined
      for thing in message.data.related-parties
        if thing.reference
          person = JSON.parse thing.reference .name

      if person
        # This can be either a create or update statement
        if message.type == "TicketChangedNotification"
          chaplin.mediator.growl.log ["#person has commented on an offer!"]
        else
          chaplin.mediator.growl.log ["#person has offered to trade with you!"]

  listings: ->

    @publish-event \change:listings

    @collection = new Listing.Collection

    $ 'footer' .show!

    @all = new chaplin.Model {
      view: \Listing
      who: \All
    }
    @view = new ListingView {
      @collection
      model: @all
      region: \all-listings
    }
    # @view.filterer = @view.filterer.bind @view
    @view.render!

    @mine = new chaplin.Model {
      view: \Listing
      who: \My
    }
    @view2 = new ListingView {
      @collection
      model: @mine
      region: \my-listings
      # +auto-render
      # filterer: filterer
    }
    # @view2.filterer = @view2.filterer.bind @view2
    @view2.render!

    @listen-to chaplin.mediator.socket, 'create:catalog', (message) ->
      @collection.unshift new @collection.model message.data

    @collection.fetch!

  offers: ->
    $ '.left' .add-class 'expanded'

    @publish-event \change:offers

    $ 'footer' .hide!

    @collection = new Ticket.Collection

    @view = new OffersView {
      @collection,
      model: @all
      region: \all-listings
      # +auto-render
    }
    @view.render!

    @listen-to chaplin.mediator.socket, 'create:ticket', (message) ->
      # This can also be an update message.  Check and see if it is first
      existing = @collection.get message.data.id

      if existing
        # it already exists, just replace the data
        existing.set message.data
      else
        # It doesn't exist, create a new one.
        @collection.add new @collection.model message.data

    @collection.fetch!

  authorization: ->
    # Parse and store the authorization parameters.
    query = parse-query-string window.location.href
    cookie.set 'authorization:token', query['#access_token']
    cookie.set 'authorization:user', query['uid']
    window.close!
