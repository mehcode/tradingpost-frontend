
require! Backbone: \backbone
require! Chaplin: \chaplin
require! _: \underscore

module.exports = class Socket implements Backbone.Events

  ->
    @socket = new WebSocket do
      'ws://ec2-54-200-196-123.us-west-2.compute.amazonaws.com/api/subscribe'

    @socket.onmessage = @~on-message
    @socket.onclose = @~on-close
    @socket.onopen = @~on-open
    @socket.onerror = @~on-error

  on-open: (message) ->
    console.log 'Socket opened', message

  on-error: (message) ->
    console.log 'Socket errored', message

  on-message: (message) ->
    # Figure out what kind of message this is
    data = JSON.parse(message.data)
    api = data.api

    console.log "New message for #api", data

    if api is 'ticket'
      # Special handling, we want to filter out everything that doesn't come
      # for us.
      id = Chaplin.mediator.user.id

      for thing in data.data.relatedParties
        if id == JSON.parse(thing.reference).id
          # This is one of ours.
          @trigger "create:#api", data
          return

      # It's not one of ours.
      return void

    # It's not the ticket api.
    @trigger "create:#api", data

  on-close: ->
    # The socket is now closed.  we should broadcast the close and die.
    @trigger \closed, this

    # Nobody has any reason to listen to us anymore
    @off!
