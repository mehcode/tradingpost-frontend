'use strict'

require! chaplin

require! HeaderView: 'views/index'
require! User: 'resources/user'

module.exports = class Index extends chaplin.Controller

  before-action: ->
    @compose 'header', ->
      @model = new User.Model
      @view = new HeaderView {@model}
      @view.render!
