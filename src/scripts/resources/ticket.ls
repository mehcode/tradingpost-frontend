'use strict'

require! Model: 'lib/models/model'
require! Collection: 'lib/collections/collection'

exports.Model = class Ticket extends Model

  name: 'ticket/'

exports.Collection = class Tickets extends Collection

  name: Ticket::name

  model: Ticket

  query:
    'type': 'TradingPost6',
    'status': 'InProgress_Pending'
