'use strict'

require! cookie
require! Model: 'lib/models/model'

exports.Model = class User extends Model

  name: 'account/info'

  bind: 'dropbox'

  cookies: false

  fetch: (options = {}) ->
    token = cookie.get 'authorization:token'
    options.headers = {} unless options.headers
    options.headers['Authorization'] = "Bearer #token"
    super options
