'use strict'

require! Model: 'lib/models/model'
require! Collection: 'lib/collections/collection'
require! CategoryResource: 'resources/product-category'

exports.Model = class ProductOffering extends Model

  name: 'product-offering/'

  initialize: (attributes = {}) ->

    console.log "MAKE ONE", attributes['productCategories']

    # Figure out the user account associated with this product offering.
    for category in (attributes['productCategories'] or [])
      let category = category
        category-model = new CategoryResource.Model {id: category.id}
        category-model.fetch!done ~>
          if (category-model.get 'name') is \user
            # WARNING: Crazy hacks beware
            data = window.JSON.parse category-model.get 'description'
            @set 'userID', data['uid']
            @set 'userEmail', data['email']
            @set 'userName', data['display_name']

    super ...

exports.Collection = class ProductOfferings extends Collection

  name: ProductOffering::name

  model: ProductOffering

  query:
    'productCategories.id': 663
