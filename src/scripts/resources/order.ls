'use strict'

require! Model: 'lib/models/model'
require! Collection: 'lib/collections/collection'

exports.Model = class ProductOrder extends Model

  name: 'order/'

exports.Collection = class ProductOrders extends Collection

  name: ProductOrder::name

  model: Model
