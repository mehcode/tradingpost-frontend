'use strict'

require! Model: 'lib/models/model'
require! Collection: 'lib/collections/collection'

exports.Model = class ProductCategory extends Model

  name: 'product-category/'

exports.Collection = class ProductCategories extends Collection

  name: ProductCategory::name

  model: Model
