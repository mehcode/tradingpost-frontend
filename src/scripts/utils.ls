'use strict'

require! $: jquery
require! cookie
require! chaplin.mediator.settings
require! chaplin.mediator
require! dropbox
require! ProductCategory: 'resources/product-category'

exports.provision-account = (user-model) ->

  deferred = new $.Deferred!

  # Attempt to get the user's category using the dropbox data store API.
  token = cookie.get 'authorization:token'
  user = cookie.get 'authorization:user'
  key = settings.codes['dropbox']
  client = new dropbox.Client {key, token}
  manager = client.getDatastoreManager!
  manager.open-default-datastore (error, store) ->
    table = store.get-table 'products'
    console.log "table", table
    results = table.query {user}
    if not results.length
      console.log "CATEGORY NOT"
      # No available category for this user; we need to make one.
      model = new ProductCategory.Model do
        name: 'user'
        description: JSON.stringify user-model

      model.save!
      .done ~>
        # We now have a category; store it in dropbox.
        table.insert {user, category: model.id}

        # Resolve.
        mediator.user.category = model.id
        mediator.user.name = user-model.get 'display_name'
        mediator.user.id = user-model.get 'uid'
        model.dispose!
        deferred.resolve!
      .fail ~>
        console.log "FAIL"

    else
      mediator.user.category = results[0].get 'category'
      mediator.user.name = user-model.get 'display_name'
      mediator.user.id = user-model.get 'uid'
      console.log "FOUND CATEGORY", mediator.user.category
      deferred.resolve!

  deferred
