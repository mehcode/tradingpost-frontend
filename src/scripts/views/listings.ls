'use strict'

require! $: jquery
require! chaplin.mediator
require! md5
require! cookie

require! CollectionView: 'lib/views/collection-view'
require! View: 'lib/views/view'
require! Chaplin: \chaplin

require! CollectionTemplate: 'templates/listings/list'
require! ItemTemplate: 'templates/listings/item'

require! Listing: 'resources/product-offering'

is-authorized = ->
  !! cookie.get 'authorization:token'

class Item extends View

  class-name: \listing

  tag-name: \li

  template: ItemTemplate

  bindings:
    'img':
      attributes: [
        * name: \src
          observe: \img
      ]

    '.name': \name

    '.description': 'description'

    '.info':
      observe: 'userName'
      visible: true
      update-view: false

    '.info .user': 'userName'

    '.info .avatar':
      attributes: [
        * name: 'src'
          observe: 'userEmail'
          on-get: (value) ->
            hash = md5.hash value
            "http://www.gravatar.com/avatar/#hash.jpg?d=identicon&s=32"
      ]

    ':el':
      'observe': ':selected',
      update-view: false,
      attributes: [
        name: 'class',
        on-get: (value) -> if value then 'selected' else null
      ]

  listen:
    'listing:selected mediator': (model, who) ->
      if who is @parent.model.get('who')
        if (@$el.css 'display') isnt \none
          if model.id isnt @model.id
            @model.set ':selected', false

    'listing:unselect-all mediator': ->
      @model.set ':selected', false
      @publish-event 'listing:unselected', @model, @parent.model.get('who')

  events:
    'click': (event) ->
      console.log "@model.serialize!", @model.serialize!
      @model.set ':selected', !!!!! not ! Boolean (@model.get ':selected')
      unless @model.get ':selected'
        @publish-event 'listing:unselected', @model, @parent.model.get('who')
      else
        @publish-event 'listing:selected', @model, @parent.model.get('who')

  initialize: (options = {}) ->
    super ...
    @parent = options.parent
    if @model.get 'productSpecification.name'
      @model.set \img, (@model.get 'productSpecification.name')
    else
      @model.set \img 'http://www.placekitten.com/160/160' unless @model.get \img
    @model.set \name 'Unnamed' unless @model.get \name

module.exports = class Listings extends CollectionView implements Chaplin.Delayer

  id: \listings

  item-view: Item

  list-selector: \ul.items

  loading-selector: \.loading

  init-item-view: (model) ->
    new @item-view {model, parent: this}

  template: CollectionTemplate

  regions:
    'user-listings': \.user-listings

  toggle-auth: ->
    @filter @filterer
    console.log "here"
    if @model.get(\who) is \My
      if is-authorized!
        console.log "SIGN IN", mediator.user.category
        @$el.show!
        $ 'footer' .show!
      else
        @$el.hide!
        $ 'footer' .hide!

    else
      if is-authorized!
        @$el.parent!parent!remove-class 'expanded'
        $ 'footer' .show!
      else
        @$el.parent!parent!add-class 'expanded'
        $ 'footer' .hide!

  filterer: (item) ~>
    # Check to see if this fulfills any search terms
    search-term = @model.get(\:search) or ''
    if search-term.length and @model.get(\who) is \All
      possibles = [item.get('name'), item.get('description'), item.get('productSpecification.description')]

      if !((_.filter possibles, -> _.contains it, search-term).length)
        # Doesn't match the search terms
        return false

    categories = item.get 'productCategories'
    for category in categories
      if category.id == mediator.user.category
        return @model.get(\who) is \My
    @model.get(\who) isnt \My

  listen:
    'user:sign-in mediator': 'toggleAuth'
    'user:sign-out mediator': 'toggleAuth'

  bindings:
    '.who': \who
    '.view': \view

    '.count':
      observe: \:count
      on-get: -> "#it available"

    '#select':
      observe: \:dropbox-select
      visible: (not)
      update-view: false

    '#select-text':
      observe: \:dropbox-select
      visible: true
      on-get: (value) -> _.truncate value, 40

    '#name':
      observe: \:name
      attributes: [
        name: \disabled
        observe: \:saving
      ]
    '#details':
      observe: \:description
      attributes: [
        name: \disabled
        observe: \:saving
      ]
    '#specification':
      observe: \:specification
      attributes: [
        name: \disabled
        observe: \:saving
      ]
    '#dropbox-select':
      observe: \:dropbox-select
      attributes: [
        name: \disabled
        observe: \:saving
      ]

    '.add-new-area, .cancel-add-area':
      observe: \who
      visible: -> switch it
      | \All => false
      | otherwise => true
      update-view: false

    '.add-listing':
      attributes: [
        observe: <[ :name :description :specification :dropbox-select ]>
        name: \disabled
        on-get: -> not(it.0 and it.1 and it.2 and it.3)
      ]

    '.search':
      observe: \who
      visible: -> switch it
      | \All => true
      | otherwise => false
      update-view: false

    '#search':
      observe: \:search
      update-view: false

  events:
    'click .add-button': ->
      @$ \.add-button .hide!
      @$ \.listing-container .hide!
      @$ \.add-form .show!
      @$ \.cancel-button .show!
      @$ \.listing-container .hide!

    'click #select': ->
      window.Dropbox.choose do
        linkType: 'direct',
        success: (files) ~>
          @model.set ':dropbox-select', files.0.link

    'click .cancel-button': ->
      @clear-form!
      @$ \.cancel-button .hide!
      @$ \.add-button .show!

    'click .add-listing': (event) ->
      event.prevent-default!

      @toggle-button!

      @model.set \:saving true
      listing = new Listing.Model do
        description: @model.get \:description
        name: @model.get \:name

      name = @model.get \:dropbox-select
      spec = {description: (@model.get \:specification), name}
      listing.set \productSpecification spec if spec?
      category = {id: mediator.settings.namespace.product}
      user-category = {id: mediator.user.category}
      listing.set \productCategories [category, user-category]
      console.log "listing.serialize!", listing.serialize!
      listing.save!done ~>
        @$ \.add-form .hide!
        @$ \.cancel-button .hide!
        @$ \.add-button .show!
        @toggle-button!
        @model.set \:saving false
        listing.dispose!
        @clear-form!

  clear-form: ->
    @$ \#name .val ''
    @$ \#dropbox-select .val ''
    @$ \#specification .val ''
    @$ \#details .val ''
    @$ \.add-form .hide!
    @$ \.listing-container .show!
    @model.set \:dropbox-select, null
    @$ \.listing-container .show!

  toggle-button: ->
    @$ \.click .toggle!
    @$ \.loader .toggle!

  render: (options = {}) ->
    super ...
    if (@model.get \who) == \My and (@model.get \view) == \Listing
      @$ \.add-new .show!

  attach: ->
    super ...
    @adjust-size!
    @toggle-auth!

  adjust-size: ->
    window-height = $ window .height!
    header-height = ($ '.left h2' .offset!.top) + ($ '.left h2' .height!)
    footer-height = ($ 'footer' .height!)
    height = window-height - header-height - 18 - footer-height
    @$ '.listing-container' .height height

  initialize: ->
    super ...

    $ window .on "resize.#{@cid}", @~adjust-size
    mediator.subscribe 'user:sign-in' ~>
      console.log "it", it

    # For some reason, chaplin.delayer's setInterval doesn't work here
    # For some reason, hooking @filter doesn't work here
    # For some reason, listening to events published by the collection doesn't
    # work here.
    # For some reason, its completely fragged if you don't do it like this.
    # I wish I knew why
    @_clearable = window.set-interval @count-models, 300

  count-models: ~>
    count = 0
    for model in @collection.models
      if @filterer model
        count++
    @model.set ':count', count

  dispose: ->
    return if @disposed
    $ window .off ".#{@cid}"
    @clear-all-timeouts!
    window.clear-interval @_clearable
    super ...
