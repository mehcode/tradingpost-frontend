'use strict'

require! $: jquery
require! chaplin.mediator
require! cookie
require! CollectionView: 'lib/views/collection-view'
require! View: 'lib/views/view'
require! CollectionTemplate: 'templates/discussion'
require! Order: 'resources/order'
require! ItemTemplate: 'templates/discussion-item'

class Item extends View

  class-name: \discussion-item

  tag-name: \li

  template: ItemTemplate

  bindings:
    '.text': 'text'

module.exports = class Discussion extends CollectionView

  id: \discussion

  animation-duration: 0

  item-view: Item

  list-selector: \ul.items

  loading-selector: \.loading

  template: CollectionTemplate

  bindings:
    'textarea': \:note

    'button.comment':
      attributes: [
        observe: \:note
        name: \disabled
        on-get: -> not it
      ]

  events:
    'submit form': (event) ->
      event.prevent-default!
      @collection.push new @collection.model do
        text: @$ 'form textarea' .val!
        author: mediator.user.name
      @$ 'form textarea' .val ''
      @model.save {notes: @collection.toJSON!} , {+patch}
      @model.set \:note, ''

    'click .accept': (event) ->
      event.prevent-default!
      data = do
        status: 'Resolved'
        status-change-reason: 'Accepted offer'
      @model.save data, {+patch} .done ~>
        # Create a new order item.
        item = new Order.Model
        item.set do
          'type': 'thetradingpost',
          'relatedParties': @model.get 'relatedParties'
          'orderItems': [
            {
              "state": "Running",
              "action": "action",
              "productOffering": {
                "id": @model.get 'relatedObjects' .0.reference
              }
            },
            {
              "state": "Running",
              "action": "action",
              "productOffering": {
                "id": @model.get 'relatedObjects' .1.reference
              }
            }
          ]

        item.save!done ~>
          @publish-event 'offer:closed', @model

    'click .reject': (event) ->
      event.prevent-default!
      data = do
        status: 'Cancelled'
        status-change-reason: 'Rejected offer'
      @model.save data, {+patch} .done ~>
        @publish-event 'offer:closed', @model

  attach: ->
    super ...
    @adjust-size!

  adjust-size: ->
    window-height = $ window .height!
    header-height = ($ '.left h2' .offset!.top) + ($ '.left h2' .height!)
    entry-height = (@$ 'form' .height!)
    height = window-height - header-height - entry-height - 11 - 50
    @$ '.discussion-container' .height height

  initialize: (options) ->
    super ...
    $ window .on "resize.#{@cid}", @~adjust-size
    @listen-to @model, 'change', ~>
      @collection.set (@model.get 'notes')
      @$ '.discussion-container' .scrollTop(@$ '.discussion-container' .0.scroll-height)

  dispose: ->
    return if @disposed
    $ window .off ".#{@cid}"
    super ...
