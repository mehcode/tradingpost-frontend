'use strict'

require! chaplin
require! chaplin.mediator
require! chaplin.helpers.redirect-to
require! chaplin.mediator.settings
require! dropbox
require! cookie
require! md5
require! utils
require! View: 'lib/views/view'
require! Template: 'templates/index'
require! ProductCollection: 'resources/product-offering'
require! Ticket: 'resources/ticket'

is-authorized = ->
  !! cookie.get 'authorization:token'

module.exports = class IndexView extends View

  id: \header

  container: \body

  template: Template

  regions:
    'my-listings': \.my-listings
    'all-listings': \.all-listings

  bindings:
    '.signin':
      observe: \display_name
      visible: -> not is-authorized!
      update-view: false

    '.offers-link':
      observe: \display_name
      visible: true
      update-view: false

    '.user-info':
      observe: \display_name
      visible: true
      update-view: false

    '.user-info .name': \display_name

    '.user-info .avatar':
      attributes: [
        * name: 'src'
          observe: 'email'
          on-get: (value) ->
            hash = md5.hash value
            "http://www.gravatar.com/avatar/#hash.jpg?d=identicon&s=32"
      ]

  _them_model = null
  _your_model = null

  update-totals: ->
    what = if @_them_model then "Selected" else 'Nothing selected'
    @$ 'footer .what' .text what

    what = if @_your_model then "Selected" else 'Nothing selected'
    @$ 'footer .for' .text what

    if @_them_model and @_your_model
      @$ 'footer' .add-class 'offer-allow'
    else
      @$ 'footer' .remove-class 'offer-allow'

  listen:
    'listing:selected mediator': (model, who) ->
      @[(if who is \My then '_your_model' else '_them_model')] = model
      @update-totals!

    'listing:unselected mediator': (model, who) ->
      @[(if who is \My then '_your_model' else '_them_model')] = null
      @update-totals!

  events:
    'click .listing-link': -> redirect-to \listings

    'click .offers-link': -> redirect-to \offers

    'click button.offer-button': (event) ->
      event.prevent-default!

      objs = []
      for model in @_your_collection.models
        objs.push {reference: model.id, involvement: cookie.get 'authorization:user'}
      for model in @_them_collection.models
        objs.push {reference: model.id, involvement: model.get 'userID'}

      parties = [
        {
          "reference": cookie.get 'authorization:user',
          "role": JSON.stringify @model
        },
      ]

      offer-model = new Ticket.Model do
        description: 'Offering'
        severity: 'Medium'
        type: 'TradingPost6'
        related-objects: [
          {
            reference: @_your_model.id,
            involvement: @_your_model.get 'userID'
          },
          {
            reference: @_them_model.id,
            involvement: @_them_model.get 'userID'
          }
        ],
        related-parties: [
          {
            "role": 'initator',
            "reference": JSON.stringify {name: @_your_model.get('userName'), email: @_your_model.get('userEmail'),, id: @_your_model.get('userID')}
          },
          {
            "reference": JSON.stringify {name: @_them_model.get('userName'), email: @_them_model.get('userEmail'), id: @_them_model.get('userID')}
            "role": 'receiver'
          }
        ]

      offer-model.save!done ~>
        @publish-event 'listing:unselect-all'

    'click .signin': (event) ->
      # Try and get an existing authoirzation id.
      if is-authorized!
        # We are now authenticated; show us.
        return

      # Build authorization URL for dropbox.
      id = settings.codes['dropbox']
      redirect = window.encodeURIComponent settings.uris['authorization']
      query = "response_type=token&client_id=#id&redirect_uri=#redirect"
      url = "https://www.dropbox.com/1/oauth2/authorize?#query"

      # Show the modal dialog.
      window.show-modal-dialog url

      # Fetch the user model.
      @model.fetch!done ~>
        mediator.user.name = @model.get 'display_name'
        utils.provision-account @model .done ~>
          @publish-event 'user:sign-in'

    'click .signout': (event) ->
      cookie.remove 'authorization:token'
      cookie.remove 'authorization:user'
      @model.clear!
      @publish-event 'user:sign-out'

  initialize: (options) ->
    # Fetch the user model if we are authorized.
    if is-authorized!
      @model.fetch!done ~>
        mediator.user.name = @model.get 'display_name'
        utils.provision-account @model .done ~>
          @publish-event 'user:sign-in'

    @_them_collection = new ProductCollection.Collection!
    @_your_collection = new ProductCollection.Collection!

    @subscribe-event \change:listings, ~>
      @$ \.offers-link .remove-class \active
      @$ \.listing-link .add-class \active
    @subscribe-event \change:offers, ~>
      @$ \.listing-link .remove-class \active
      @$ \.offers-link .add-class \active
