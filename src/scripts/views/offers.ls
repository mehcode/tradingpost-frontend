'use strict'

require! $: jquery
require! chaplin.mediator
require! cookie
require! CollectionView: 'lib/views/collection-view'
require! View: 'lib/views/view'
require! Collection: 'lib/collections/collection'
require! DiscussionView: 'views/discussion'
require! CollectionTemplate: 'templates/offers/list'
require! ItemTemplate: 'templates/offers/item'
require! ProductResource: 'resources/product-offering'

class Item extends View

  class-name: \offer

  tag-name: \li

  template: ItemTemplate

  bindings:
    '.them img':
      attributes: [
        * name: \src
          observe: 'them_Image'
      ]

    '.them .name': \you_name

    '.you img':
      attributes: [
        * name: \src
          observe: 'you_Image'
      ]

    '.you .name': \you_name

    '.them .info .name':
      observe: 'relatedParties.1.reference'
      on-get: (text) ->
        data = JSON.parse text
        data.name

    ':el':
      'observe': ':selected',
      update-view: false,
      attributes: [
        name: 'class',
        on-get: (value) -> if value then 'selected' else null
      ]

  listen:
    'order:selected mediator': (model) ->
      if model.id isnt @model.id
        @model.set ':selected', false

  events:
    'click': (event) ->
      @model.set ':selected', !!!!! not ! Boolean (@model.get ':selected')
      unless @model.get ':selected'
        @publish-event 'order:unselected', @model
      else
        @publish-event 'order:selected', @model

  initialize: (options = {}) ->
    super ...
    @parent = options.parent
    @model.set \them_Image 'http://library.binus.ac.id/images/noImageAvailable.jpg' unless @model.get \img
    @model.set \you_Image 'http://library.binus.ac.id/images/noImageAvailable.jpg' unless @model.get \img

    # Get products
      id = @model.get 'relatedObjects.1.reference'
    them-product = new ProductResource.Model {id}
    them-product.fetch!done (data) ~>
      if data.productSpecification.name
        @model.set \them_Image, data.productSpecification.name

    id = @model.get 'relatedObjects.0.reference'
    your-product = new ProductResource.Model {id}
    your-product.fetch!done (data) ~>
      if data.productSpecification.name
        @model.set \you_Image, data.productSpecification.name

module.exports = class Offers extends CollectionView

  id: \offers

  item-view: Item

  list-selector: \ul.items

  loading-selector: \.loading

  filterer: (item) ->
    name = mediator.user.id
    for thing in item.get 'relatedParties'
      console.log thing.reference
      if thing.reference and JSON.parse thing.reference .id == name
        return true

    return false

  listen:
    'order:selected mediator': (model) ->
      disc-items = new Collection model.get('notes')
      @disc-view.dispose! if @disc-view
      @disc-view = new DiscussionView {
        +auto-render,
        model,
        collection: disc-items
        region: \my-listings
      }
      $ '.left' .remove-class 'expanded'

    'order:unselected mediator': (model) ->
      @disc-view.dispose! if @disc-view
      $ '.left' .add-class 'expanded'

    'user:sign-in mediator': ->
      @filter @filterer

    'offer:closed mediator': (model) ->
      @collection.remove model
      @disc-view.dispose! if @disc-view
      $ '.left' .add-class 'expanded'

  init-item-view: (model) ->
    new @item-view {model, parent: this}

  template: CollectionTemplate

  attach: ->
    super ...
    @adjust-size!

  adjust-size: ->
    window-height = $ window .height!
    header-height = ($ '.left h2' .offset!.top) + ($ '.left h2' .height!)
    height = window-height - header-height - 16
    @$ '.offer-container' .height height

  initialize: ->
    super ...
    $ window .on "resize.#{@cid}", @~adjust-size
    mediator.subscribe 'user:sign-in' ~>
      console.log "it", it
      # @listen-to @collection, 'sync add remove reset', ~>
      #   @filter! @filterer

  dispose: ->
    return if @disposed
    $ window .off ".#{@cid}"
    @disc-view.dispose! if @disc-view
    super ...
