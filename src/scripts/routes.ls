'use strict'

module.exports = (route) ->
  route '', 'index#listings'
  route 'listings', 'index#listings', {name: \listings}
  route 'offers', 'index#offers', {name: \offers}
  route 'authorization', 'index#authorization'